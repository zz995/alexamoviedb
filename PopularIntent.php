<?php

namespace movieDB;

use AlexaPHPSDK\Intent;
use AlexaPHPSDK\Response;
use AlexaPHPSDK\Skill;

//REGION Region

class PopularIntent extends Intent {

    function __construct() {
        $this->response = new Response();
        $this->apiConfig = Skill::getInstance()['api'];
    }

    protected function getTitlesFilms($region) {
        $mapRegion = [
            'australia' => 'AU',
            'ukraine' => 'UA',
            'united states of america' => 'US'
        ];
        $regionIso = @$mapRegion[strtolower($region)];
        $url = $this->apiConfig['url'] . 'movie/popular?api_key=' . urlencode($this->apiConfig['key']);
        if (isset($regionIso)) {
            $url .= '&region=' . urlencode($regionIso);
        }

        $rawData = file_get_contents($url);
        if (empty($rawData)) {
            return [];
        }

        $jsonData = json_decode($rawData);
        $films = @$jsonData->results;
        if (empty($films)) {
            return [];
        }

        $titles = array_map(function ($film) { return $film->title; }, $films);
        return $titles;
    }

    private function setResponse($params) {
        $titlesFilms = $this->getTitlesFilms($params['region']);
        $textResponse = implode(', ', $titlesFilms);
        if (empty($textResponse)) {
            $textResponse = 'Sorry, I don\'t know popular films';
        }
        $this->response->addText($textResponse);
    }

    public function ask($params = array()) {
        $this->setResponse($params);
        $this->response->forceSessionEnd();
        return $this->response;
    }

    public function run($params = array()) {
        $this->setResponse($params);
        $this->response->forceSessionEnd();
        return $this->response;
    }
    
}