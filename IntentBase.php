<?php

namespace movieDB;

use AlexaPHPSDK\Intent;
use AlexaPHPSDK\Response;
use AlexaPHPSDK\Skill;
use AlexaPHPSDK\User;
use Language\Language;
use Request\ErrorHandler;
use Request\Request;


class IntentBase extends Intent {

    const TYPE_REQUEST_ASK = 0;
    const TYPE_REQUEST_RUN = 1;

    public $request;

    public function __construct(User $user) {

        require __DIR__ . '/vendor/autoload.php';
        $errorHandler = new ErrorHandler();
        $errorHandler->register();

        parent::__construct($user);
        $this->request = new Request();
        $this->response = new Response();
        $this->languageStrings = new Language();
        $this->apiConfig = Skill::getInstance()['api'];
    }

    public function action($params, int $typeRequest): Response {
        return $this->endSessionResponse('Goodbye.');
    }

    public function ask($params = array()) {
        return $this->action($params, self::TYPE_REQUEST_ASK);
    }

    public function run($params = array()) {
        return $this->action($params, self::TYPE_REQUEST_RUN);
    }
}