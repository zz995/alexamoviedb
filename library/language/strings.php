<?php

return [
    'en' => [
        'NOT_SURE' => 'Sorry, I\'m not sure about %s you can try again'
    ],
    'en-US' => [
        'NOT_KNOW' => 'Sorry, I don\'t know %s'
    ]
];