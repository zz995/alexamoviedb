<?php

namespace Language;

class Language {
    protected $language = [];
    protected $strings;

    public function __construct(string $default = null) {
        $this->setDefault($default);
        $this->strings = require_once(__DIR__ . '/strings.php');
    }

    public function setDefault(string $default = null) {
        $this->language = isset($default) ? explode('-', $default) : [];
        if (!isset($this->language[0])) {
            $this->language[0] = 'en';
        }
        if (!isset($this->language[1])) {
            $this->language[1] = 'US';
        }
    }

    public function get(string $template, array $params = null): string {
        $string = @$this->strings[implode('-', $this->language)][$template];
        if (is_null($string)) {
            $string = @$this->strings[$this->language[0]][$template];
        }
        if (isset($string)) {
            return call_user_func_array("sprintf", array_merge([$string], $params));
        }

        return '';
    }
}