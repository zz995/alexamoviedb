<?php

namespace Api;

use Url\Url;

class TheMovieDBApi {
    protected $config;

    public function __construct(array $config) {
        $this->config = $config;
    }

    public function nowPlayingMovieTitles($regionIso = null) {
        $films = $this->nowPlayingMovie($regionIso);
        if (is_null($films)) return null;

        $titles = array_map(function ($film) { return $film->title; }, $films);
        return $titles;
    }

    public function nowPlayingMovie($regionIso = null) {
        $url = (new Url($this->config['url']))
            ->addPath('movie/now_playing')
            ->addParam('api_key', $this->config['key']);
        isset($regionIso) and $url->addParam('region', $regionIso);
        return self::request($url);
    }

    public static function request(Url $url) {
        $rawData = file_get_contents($url->getUrl());
        if (empty($rawData)) {
            return null;
        }

        $jsonData = json_decode($rawData);
        $result = @$jsonData->results;
        if (empty($result)) {
            return null;
        }

        return $result;
    }
}