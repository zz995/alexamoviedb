<?php

namespace Request;

class ErrorHandler {

    public function register() {
        set_error_handler(array($this, 'errorHandler'));
        set_exception_handler(array($this, 'exceptionHandler'));
        register_shutdown_function(array($this, "registerShutdownFunction"));
    }

    public function errorHandler($errno, $errstr, $errfile = '', $errline = '') {
        if(!(error_reporting() & $errno)){
            return;
        }
        $msg = implode(' ', [$errno, $errstr, $errfile, $errline]);
        return false;
    }

    public function exceptionHandler($exception) {
        $msg = $exception->getMessage() . ' : ' .  $exception->getFile() . ':' .  $exception->getLine() . $exception->getTraceAsString();
        $this->errorHandler(E_ERROR, $msg);
        return null;
    }

    public function registerShutdownFunction() {
        $lasterror = error_get_last();
        $this->errorHandler($lasterror['type'], $lasterror['message'], $lasterror['file'], $lasterror['line']);
    }
}