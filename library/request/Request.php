<?php

namespace Request;

class Request {
    protected $_params = [];
    protected $_postDataJsonRaw = null;
    protected $_locale = null;

    public function __construct() {
        $postData = file_get_contents("php://input");
        $this->_postDataJsonRaw = empty($postData) ? null : json_decode($postData, true);
        $this->_locale = @$this->_postDataJsonRaw['request']['locale'] ?: 'en-US';

        foreach($this->getSlotsJsonRaw() as $name => $slot) {
            $this->_params[$name] = new Slot($slot);
        }
    }

    public function hasSlot(string $name): bool {
        return array_key_exists($name, $this->_params) && $this->getSlot($name)->isExist();
    }

    public function getLocal(): string {
        return $this->_locale;
    }

    public function getSlot(string $name): Slot {
        return $this->_params[$name];
    }

    protected function getSlotsJsonRaw(): array {
        return @$this->_postDataJsonRaw['request']['intent']['slots'] ?: [];
    }
}