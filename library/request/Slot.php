<?php

namespace Request;

class Slot {
    protected $_statusCode;
    protected $_name;
    protected $_value;
    protected $_id;

    public function __construct($requestParsedData) {
        $this->_statusCode = @$requestParsedData['resolutions']['resolutionsPerAuthority'][0]['status']['code'];
        $this->_name = @$requestParsedData['name'];
        $this->_value = @$requestParsedData['value'];
        $this->_id = @$requestParsedData['resolutions']['resolutionsPerAuthority'][0]['values'][0]['value']['id'];
    }

    public function getId() {
        return $this->_id;
    }

    public function getName() {
        return $this->_name;
    }

    public function getValue() {
        return $this->_value;
    }

    public function isExist() {
        return isset($this->_value);
    }

    public function isMatch() {
        return $this->_statusCode == 'ER_SUCCESS_MATCH';
    }
}