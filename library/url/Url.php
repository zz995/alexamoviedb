<?php

namespace Url;

class Url {
    protected $url;
    protected $params = [];

    public function __construct(string $path) {
        $this->url = parse_url($path);
        $query = isset($this->url['query']) ? $this->url['query'] : '';
        parse_str($query,$this->params);
    }

    public function setHost(string $host): Url {
        $this->url['host'] = $host;

        return $this;
    }

    public function addPath(string  $partPath): Url {
        if (substr($partPath, 0, 1) == '/') {
            $this->url['path'] = $partPath;
        } else {
            $this->url['path'] = rtrim($this->url['path'], '/') . '/' . $partPath;
        }

        return $this;
    }

    public function addParam(string $key, string $value): Url {
        $this->params[$key] = $value;

        return $this;
    }

    public function getUrl(): string {
        $result = '';
        $query = http_build_query($this->params);

        if (isset($this->url['scheme'])) {
            $result .= $this->url['scheme'] . ':';
        }

        if (isset($this->url['host'])) {
            $result .= '//' . $this->url['host'];
        }

        if (isset($this->url['path'])) {
            $result .= $this->url['path'];
        }

        if(!empty($query)) {
            $result .= '?' . $query;
        }

        return $result;
    }
}