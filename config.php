<?php
/**
 * Created by zheka on 03/02/2018 14:49:34
*/

return array (
    'directories' => array (
        'content' => __DIR__.'/content',
    ),
    'skillHttpsUrl' => 'https://aa95c399.ngrok.io/alexa/movieDB',
    'allowedContentTypes' => 'jpg|jpeg|gif|mp3',
    'api' => [
        'key' => '7b45a05d64cb11c6faf516b6ef91c69f',
        'url' => 'https://api.themoviedb.org/3/',
        'img' => 'https://image.tmdb.org/t/p/w600_and_h900_bestv2'
    ]
);