<?php

namespace movieDB;

use AlexaPHPSDK\Intent;
use AlexaPHPSDK\Response;
use AlexaPHPSDK\Skill;
use AlexaPHPSDK\User;

//AMAZON.SearchQuery Film

class FilmInfoIntent extends Intent {

    function __construct(User $user) {
        $this->response = new Response();
        $this->apiConfig = Skill::getInstance()['api'];
        parent::__construct($user);
    }

    protected function searchFilm($filmTitle) {
        $filmTitle = trim($filmTitle);
        if (empty($filmTitle)) {
            return [];
        }

        $url = $this->apiConfig['url'] . 'search/movie?api_key=' . urlencode($this->apiConfig['key']);
        $url .= '&query=' . urlencode($filmTitle);

        $rawData = file_get_contents($url);
        if (empty($rawData)) {
            return [];
        }

        $jsonData = json_decode($rawData);
        $films = @$jsonData->results;
        if (empty($films)) {
            return [];
        }

        return $films;
    }

    public function ask($params = array()) {
        $films = $this->searchFilm($params['film']);
        $film = @$films[0];

        if (is_null($film)) {
            return $this->notKnow();
        }

        $this->setCard($film);
        $this->response->addText($film->overview);
        $this->response->forceSessionEnd(true);

        return $this->response;
    }
    
    public function run($params = array()) {
        $films = $this->searchFilm($params['film']);
        $film = @$films[0];

        if (is_null($film)) {
            return $this->notKnow();
        }

        $user = $this->user;
        $user['movieId'] = $film->id;

        $this->setCard($film);
        $this->response->addText($film->overview);

        return $this->response;
    }

    protected function setCard($film) {
        $title = $film->title;
        $desc = $film->overview;
        $imgPath = null;
        if (isset($film->poster_path)) {
            $imgPath = $this->apiConfig['img'] . $film->poster_path;
        }
        $this->response->setDescription($desc, $title);
        isset($imgPath) and $this->response->setImage($imgPath);
    }

    protected function notKnow() {
        $this->response->addText('Sorry, I don\'t know about that film');
        return $this->response;
    }
}