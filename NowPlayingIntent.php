<?php

namespace movieDB;

use AlexaPHPSDK\Intent;
use AlexaPHPSDK\Response;
use AlexaPHPSDK\Skill;
use AlexaPHPSDK\User;
use Api\TheMovieDBApi;

//REGION Region

class NowPlayingIntent extends IntentBase {

    function __construct(User $user) {
        parent::__construct($user);
        $this->movieGateway = new TheMovieDBApi(Skill::getInstance()['api']);
    }

    public function action($params, int $typeRequest): Response {
        $request = $this->request;
        $regionIso = null;
        $user = $this->user;

        $attempt = @$user['attempt'] ?: 0;
        $user['attempt'] = $attempt + 1;

        $hasRegion = $request->hasSlot('Region');
        $matchRegion = $hasRegion and $request->getSlot('Region')->isMatch();

        if ($hasRegion && !$matchRegion && $attempt < 2) {
            $this->response->addText($this->languageStrings->get('NOT_SURE', ['region']));
            return $this->response;
        }

        $user['attempt'] = 0;
        if ($hasRegion && !$matchRegion) {
            $regionIso = @explode('-', $this->request->getLocal())[1];
        }

        $titlesFilms = $this->movieGateway->nowPlayingMovieTitles($regionIso);
        if (empty($titlesFilms)) {
            $this->response->addText($this->languageStrings->get('NOT_KNOW', ['films']));
            $this->response->setDescription($this->languageStrings->get('NOT_KNOW', ['films']));
        } else {
            $this->response->addText(implode(', ', array_slice($titlesFilms, 0, 5)));
            $this->response->setDescription(implode(', ', $titlesFilms));
        }

        $this->response->forceSessionEnd();

        return $this->response;
    }
}