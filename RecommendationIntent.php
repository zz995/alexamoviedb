<?php

namespace movieDB;

use AlexaPHPSDK\Intent;
use AlexaPHPSDK\Response;
use AlexaPHPSDK\Skill;
use AlexaPHPSDK\User;

//NO SLOTS

class RecommendationIntent extends Intent {

    function __construct(User $user) {
        $this->response = new Response();
        $this->apiConfig = Skill::getInstance()['api'];
        parent::__construct($user);
    }

    protected function recommendationFilm($movieId, $index = 0) {
        if(is_null($movieId)) {
          return null;
        }

        $url = $this->apiConfig['url'] . 'movie/' . $movieId . '/recommendations?api_key=' . urlencode($this->apiConfig['key']);

        $rawData = file_get_contents($url);
        if (empty($rawData)) {
            return [];
        }

        $jsonData = json_decode($rawData);
        $recommendations = @$jsonData->results;
        if (empty($recommendations)) {
            return [];
        }

        return @$recommendations[$index];
    }

    public function ask($params = array()) {
        return $this->endSessionResponse('Goodbye.');
    }

    public function run($params = array()) {

        $user = $this->user;
        $index = $user['index'] ?: 0;
        $user['index'] = $index + 1;

        $movieId = $user['movieId'];
        $recommendation = $this->recommendationFilm($movieId, $index);

        if (is_null($recommendation)) {
            $this->response->addText('Sorry, I don\'t know recommendations');
            return $this->response;
        }

        $this->response->addText($recommendation->overview);

        return $this->response;
        //return $this->endSessionResponse('Goodbye.');
    }
    
}